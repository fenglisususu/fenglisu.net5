﻿using System;

namespace Fenglisu.Base.Events
{
    public interface IEventEntity
    {
        string EventId { get; set; }
        string EventType { get; set; }
        string EventData { get; set; }
        DateTime CreatedAt { get; set; }
    }
}
