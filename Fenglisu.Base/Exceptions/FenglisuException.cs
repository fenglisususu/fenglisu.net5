﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fenglisu.Base.Exceptions
{
    public class FenglisuException : Exception
    {
        public FenglisuException(string message) : base(message)
        {

        }
    }
}
