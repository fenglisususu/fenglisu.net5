﻿using System;

namespace Fenglisu.Base.Exceptions
{
    public class NoPermissionException : AspireException
    {
        public NoPermissionException() : base(NOPERMISSION)
        {

        }

        public NoPermissionException(string message, string code = "", Action action = null) : base(message, code, action)
        {
        }
    }
}
