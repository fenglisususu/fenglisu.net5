﻿using System;

namespace Fenglisu.Base.Models
{
    public class BaseGuidModel
    {
        public Guid Id { get; set; }
    }
}
