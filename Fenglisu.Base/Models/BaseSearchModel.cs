﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fenglisu.Base.Models
{
    public class BaseSearchModel : PageModel
    {
        public string SearchText { get; set; }
    }
}
