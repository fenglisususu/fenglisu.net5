﻿namespace Fenglisu.Base.ObjectStorage
{
    public interface IObjectStorage
    {
        string Upload(string key, byte[] data);
    }
}
