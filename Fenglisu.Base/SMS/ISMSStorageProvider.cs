﻿
using System;

namespace Fenglisu.Base.SMS
{
    public interface ISMSStorageProvider
    {
        void Save(string productName, string content, string phoneNumbers, DateTime sendTime, bool success, string message,
            string response);
    }
}
