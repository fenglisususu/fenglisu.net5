﻿using System;
using Fenglisu.Base.Logging;
using Newtonsoft.Json;

namespace Fenglisu.Base.SMS
{
    public class NullSMSStorageProvider : ISMSStorageProvider
    {

        public NullSMSStorageProvider()
        {
        }

        public void Save(string productName, string content, string phoneNumbers, DateTime sendTime, bool success, string message, string response)
        {
        }
    }
}
