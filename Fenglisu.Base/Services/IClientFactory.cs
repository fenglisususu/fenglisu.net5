﻿using System.Threading.Tasks;

namespace Fenglisu.Base.Services
{
    public interface IClientFactory
    {
        Task<T> Create<T>() where T : IService;
    }
}
