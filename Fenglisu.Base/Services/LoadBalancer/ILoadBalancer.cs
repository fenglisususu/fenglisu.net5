﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Fenglisu.Base.Services.Registry;

namespace Fenglisu.Base.Services.LoadBalancer
{
    public interface ILoadBalancer
    {
        Task<ServiceInformation> Select(IList<ServiceInformation> services);
    }
}
