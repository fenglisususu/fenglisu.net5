﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fenglisu.Base.Services.Registry;

namespace Fenglisu.Base.Services.LoadBalancer
{
    public class RandomLoadBalancer : ILoadBalancer
    {
        private readonly Random _random = new Random();

        public Task<ServiceInformation> Select(IList<ServiceInformation> services)
        {
            return Task.FromResult(services == null || services.Count == 0 ? null : services[_random.Next(services.Count)]);
        }
    }
}
