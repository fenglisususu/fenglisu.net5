﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fenglisu.Base.Services.Registry
{
    public interface IServiceRegistry
    {
        Task<ServiceInformation> Register(string serviceName, string serviceHost, int servicePort, string healthCheckUrl = null);

        Task Deregister(string serviceId);

        Task<IList<ServiceInformation>> FindServices(string name);
    }
}
