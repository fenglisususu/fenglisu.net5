﻿using Microsoft.Extensions.DependencyInjection;

namespace Fenglisu.Consul
{
    public class ConsulBuilder
    {
        public IServiceCollection Services { get; }

        public ConsulBuilder(IServiceCollection services)
        {
            Services = services;
        }
    }
}
