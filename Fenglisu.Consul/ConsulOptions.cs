﻿namespace Fenglisu.Consul
{
    public class ConsulOptions
    {
        public string HttpEndpoint { get; set; } = "http://localhost:8500";
        public string Datacenter { get; set; }
    }
}
