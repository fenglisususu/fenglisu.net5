﻿using Fenglisu.Base.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Fenglisu.EF
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEntityFrameworkRepositoryContext : IRepositoryContext
    {
        /// <summary>
        /// 
        /// </summary>
        DbContext Context { get; }
    }
}
