﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Fenglisu.ElasticSearch
{
    public static class ElasticSearchExtensions
    {
        public static IServiceCollection AddRedisCache(this IServiceCollection services, IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            services.Configure<ElasticSearchOptions>(configuration);
            services.AddSingleton<IElasticSearchManager, ElasticSearchManager>();

            return services;
        }
    }
}
