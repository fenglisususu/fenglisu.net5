﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Nest;

namespace Fenglisu.ElasticSearch
{
    public interface IElasticSearchManager
    {

    }

    public class ElasticSearchManager : IElasticSearchManager
    {
        private readonly IElasticClient _elasticClient;
        public ElasticSearchManager(IOptions<ElasticSearchOptions> optionsAccessor)
        {
            var options = optionsAccessor.Value;
            var connectionSettings = new ConnectionSettings(new Uri(options.Urls));
            _elasticClient = new ElasticClient(connectionSettings);
        }
    }
}
