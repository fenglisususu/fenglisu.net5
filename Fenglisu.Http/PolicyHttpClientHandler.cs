﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Fenglisu.Base.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Polly;
using Polly.Retry;

namespace Fenglisu.Http
{
    public class PolicyHttpClientHandler : HttpClientHandler
    {
        private static readonly AsyncRetryPolicy _policy;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;

        static PolicyHttpClientHandler()
        {
            // 重试策略
            _policy = Policy.Handle<Exception>(i => !i.GetType().IsAssignableFrom(typeof(AspireException))).RetryAsync(3);
        }

        public PolicyHttpClientHandler( IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var apiAddress = string.Format("{0} {1}://{2}:{3}{4}", request.Method, request.RequestUri.Scheme,
                request.RequestUri.Host, request.RequestUri.Port, request.RequestUri.PathAndQuery);


            if (_httpContextAccessor.HttpContext != null)
            {
                var requestHeaders = _httpContextAccessor.HttpContext.Request.Headers;
                IDictionary<string, string> headers = new Dictionary<string, string>();
                foreach (var pair in requestHeaders)
                {
                    var headerName = pair.Key;
                    if (headerName.EndsWith("token", StringComparison.CurrentCultureIgnoreCase))
                        headers.Add(headerName, requestHeaders[headerName]);
                }
                var appId = _configuration["AppId"];
                if (!string.IsNullOrEmpty(appId))
                    headers.Add("X-App-Id", appId);

                if (headers.Count > 0)
                {
                    foreach (var headerName in headers.Keys)
                    {
                        request.Headers.Add(headerName, headers[headerName]);
                    }
                }
            }

            Stopwatch stopwatch = Stopwatch.StartNew();
            var response = await _policy.ExecuteAsync(() => base.SendAsync(request, cancellationToken));
            stopwatch.Stop();

            //ExceptionlessClient.Default.CreateLog("HTTP", apiAddress, LogLevel.Debug)
            //    .SetProperty("Headers", request.Headers).SetProperty("Elapsed", stopwatch.ElapsedMilliseconds).Submit();

            return response;
        }
    }
}
