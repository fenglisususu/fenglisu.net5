﻿namespace Fenglisu.Log
{
    public class Log4NetOptions
    {
        public string ConfigPath { get; set; } = "log4net.config";
    }
}
