﻿using System;
using Fenglisu.Base.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace Fenglisu.Log
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddLog4NetLogger(this IServiceCollection services,
            Action<Log4NetOptions> options)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            services.Configure(options);
            services.AddSingleton<ILogger, Log4Logger>();

            return services;
        }
    }
}
