﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using RedLockNet.SERedis;
using RedLockNet.SERedis.Configuration;
using StackExchange.Redis;
using Fenglisu.Base.Caching;
using Newtonsoft.Json;

namespace Fenglisu.Redis
{
    /// <summary>
    /// Redis缓存
    /// </summary>
    public class RedisCacheManager : ICacheManager
    {
        public readonly ConnectionMultiplexer Connection;
        public readonly IDatabase Database;
        private RedLockFactory _redLockFactory;

        public RedisCacheManager(IOptions<RedisOptions> optionsAccessor)
        {

            var options = optionsAccessor.Value;
            if (options.Endpoints == null || options.Endpoints.Length == 0)
                throw new ArgumentNullException(nameof(options.Endpoints));

            var configurationOptions = new ConfigurationOptions
            {
                AllowAdmin = options.AllowAdmin,
                AbortOnConnectFail = options.AbortOnConnectFail,
                ConnectTimeout = options.ConnectTimeout,
                SyncTimeout = options.SyncTimeout,
                DefaultDatabase = options.Db
            };
            foreach (var endpoint in options.Endpoints)
            {
                configurationOptions.EndPoints.Add(endpoint);
            }

            Connection = ConnectionMultiplexer.Connect(configurationOptions);
            Connection.IncludeDetailInExceptions = true;
            Connection.ErrorMessage += ConnectionOnErrorMessage;
            Connection.InternalError += ConnectionOnInternalError;

            Database = Connection.GetDatabase(options.Db);

            var multiplexers = new List<RedLockMultiplexer> { Connection };
            _redLockFactory = RedLockFactory.Create(multiplexers);

        }

        private void ConnectionOnInternalError(object sender, InternalErrorEventArgs e)
        {
        }

        private void ConnectionOnErrorMessage(object sender, RedisErrorEventArgs e)
        {
        }

        public T Get<T>(string key)
        {
            var value = Database.StringGet(key);
            try
            {
                if (value.HasValue)
                    return JsonConvert.DeserializeObject<T>(value);
                return default(T);
            }
            catch (Exception exception)
            {
                return default(T);
            }
        }

        public T Get<T>(string key, Func<T> func)
        {
            try
            {
                T value;
                var obj = Database.StringGet(key);
                if (obj.IsNull)
                {
                    if (func != null)
                    {
                        value = func.Invoke();
                        Database.StringSet(key, JsonConvert.SerializeObject(value));
                        return value;
                    }
                    return default(T);
                }
                value = JsonConvert.DeserializeObject<T>(obj);
                return value;
            }
            catch (Exception exception)
            {
                return default(T);
            }
        }

        public void Set(string key, object value, int minutes = 0)
        {
            var serializedValue = JsonConvert.SerializeObject(value);
            TimeSpan? timeSpan = null;
            if (minutes > 0)
            {
                timeSpan = TimeSpan.FromMinutes(minutes);
            }
            Database.StringSet(key, serializedValue, timeSpan);
        }

        public bool IsSet(string key)
        {
            return Database.KeyExists(key);
        }

        public List<T> GetByPattern<T>(string pattern)
        {
            var list = new List<T>();

            var endPoints = Connection.GetEndPoints();
            if (endPoints.Length > 0)
            {

                var server = Connection.GetServer(endPoints[0]);
                var keys = server.Keys(pattern: string.Concat('*', pattern, '*'), pageSize: 100);

                foreach (var redisKey in keys)
                {
                    var t = Get<T>(redisKey);
                    list.Add(t);
                }
            }

            return list;
        }

        public void Remove(string key)
        {
            if (key != null)
                Database.KeyDelete(key);
        }

        public void RemoveByPattern(string pattern)
        {
            var endPoints = Connection.GetEndPoints();
            if (endPoints.Length > 0)
            {
                var server = Connection.GetServer(endPoints[0]);
                var keys = server.Keys(pattern: string.Concat('*', pattern, '*'), pageSize: 100);
                Database.KeyDelete(keys.ToArray());
            }
        }

        public void HashSet(string key, string hashkey, object value)
        {
            var serializedValue = JsonConvert.SerializeObject(value);
            Database.HashSet(key, hashkey, serializedValue);
        }

        public void HashSet<T>(string key, List<T> values) where T : Iid
        {
            var list = new List<HashEntry>();
            foreach (var value in values)
            {
                HashEntry e = new HashEntry(value.Id.ToString(), JsonConvert.SerializeObject(value));
                list.Add(e);
            }
            Database.HashSet(key, list.ToArray());
        }

        public void HashRemove(string key, string hashkey)
        {
            Database.HashDelete(key, hashkey);
        }

        public void HashRemove(string key, List<string> hashkeys)
        {
            var hashs = hashkeys.Select(s => (RedisValue)s).ToArray();
            Database.HashDelete(key, hashs);
        }

        public T HashGet<T>(string key, string hashkey)
        {
            if (string.IsNullOrWhiteSpace(hashkey)) return default(T);

            var value = Database.HashGet(key, hashkey);
            try
            {
                if (value.HasValue)
                    return JsonConvert.DeserializeObject<T>(value);
                return default(T);
            }
            catch (Exception exception)
            {
                return default(T);
            }
        }
        public List<T> HashGet<T>(string key, List<string> hashkeys)
        {
            var result = new List<T>();
            var values = Database.HashGetAll(key);
            try
            {
                foreach (var hashEntry in values)
                {
                    if (hashEntry.Value.HasValue)
                        if (hashkeys.Contains(hashEntry.Name))
                            result.Add(JsonConvert.DeserializeObject<T>(hashEntry.Value));
                }
                return result;
            }
            catch (Exception exception)
            {
                return new List<T>();
            }
        }
        public List<T> HashGet<T>(string key)
        {
            var result = new List<T>();
            var values = Database.HashGetAll(key);
            try
            {
                foreach (var hashEntry in values)
                {
                    if (hashEntry.Value.HasValue)
                        result.Add(JsonConvert.DeserializeObject<T>(hashEntry.Value));
                }
                return result;
            }
            catch (Exception exception)
            {
                return new List<T>();
            }
        }

        public bool HashExists(string key, string hashkey)
        {
            return Database.HashExists(key, hashkey);
        }

        public long Increment(string key, int value = 1)
        {
            return Database.StringIncrement(key, value);
        }

        public void Lock(string resource, TimeSpan expiry, Action action)
        {
            // resource 锁定的对象
            // expiryTime 锁定过期时间，锁区域内的逻辑执行如果超过过期时间，锁将被释放
            // waitTime 等待时间,相同的 resource 如果当前的锁被其他线程占用,最多等待时间
            // retryTime 等待时间内，多久尝试获取一次
            var wait = TimeSpan.FromSeconds(10);
            var retry = TimeSpan.FromSeconds(1);
            using (var redLock = _redLockFactory.CreateLock(resource, expiry, wait, retry))
            {
                if (redLock.IsAcquired)
                {
                    action();
                }
            }
        }

        public string StringGet(string key)
        {
            var value = Database.StringGet(key);
            return value;
        }

        public void StringSet(string key, string data, int minutes = 0)
        {
            TimeSpan? timeSpan = null;
            if (minutes > 0)
            {
                timeSpan = TimeSpan.FromMinutes(minutes);
            }
            Database.StringSet(key, data, timeSpan);
        }
    }
}
