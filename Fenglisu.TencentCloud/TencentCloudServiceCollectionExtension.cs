﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fenglisu.Base.ObjectStorage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Fenglisu.TencentCloud
{
    public static class TencentCloudServiceCollectionExtension
    {
        public static IServiceCollection AddQCloudObjectStorage(this IServiceCollection services, Action<TencentCloudOptions> options)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            services.Configure(options);
            services.AddSingleton<IObjectStorage, TencentObjectStorage>();

            return services;
        }

        public static IServiceCollection AddQCloudObjectStorage(this IServiceCollection services,
            IConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            services.Configure<TencentCloudOptions>(configuration);
            services.AddSingleton<IObjectStorage, TencentObjectStorage>();

            return services;
        }
    }
}
