﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COSXML;
using COSXML.Auth;
using COSXML.Model.Object;
using COSXML.Model.Bucket;
using COSXML.CosException;
using COSXML.Utils;
using Fenglisu.Base.Logging;
using Fenglisu.Base.ObjectStorage;
using Microsoft.Extensions.Options;


namespace Fenglisu.TencentCloud
{
    public class TencentObjectStorage : IObjectStorage
    {
        private readonly TencentCloudOptions _options;
        private readonly CosXmlServer _server;
        public TencentObjectStorage(IOptions<TencentCloudOptions> optionsAccessor)
        {
            _options = optionsAccessor.Value;
            CosXmlConfig config = new CosXmlConfig.Builder().IsHttps(true).SetAppid(_options.AppId).SetRegion(_options.Region).SetDebugLog(true).Build();
            QCloudCredentialProvider cosCredentialProvider = new DefaultQCloudCredentialProvider(_options.SecretId, _options.SecretKey, 7200);
            _server = new CosXmlServer(config, cosCredentialProvider);
        }

        public string Upload(string key, byte[] data)
        {
            PostObjectRequest request = new PostObjectRequest(_options.Bucket, key, data);
            //request.SetSign(TimeUtils.GetCurrentTime(TimeUnit.Milliseconds), 600);

            var result = _server.PostObject(request);
            return result.responseHeaders["Location"][0];
        }
    }
}
